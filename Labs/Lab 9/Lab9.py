##Schuyler Vetry
##lab 9
##CMPSC 300
## April 22


import Bio.PDB
import numpy 

from Bio.PDB import PDBParser

parser = PDBParser(PERMISSIVE=1)

structure = parser.get_structure('1kfj','1kfj.pdb')

coordn = 0
coorda = 0
distance = 0

distancelist = []

for model in structure:
	print model
	for chain in model:
		print chain
		for residue in chain:
			for atom in residue:
				if atom.name == 'N': 
					coordn = atom
				elif atom.name == 'CA':
					coorda = atom
			distance = coordn - coorda
			distancelist.append(distance)

print len(distancelist)

import pylab
pylab.hist(distancelist, bins=20)
pylab.title('Alpha Carbon to Nitrogen distances')
pylab.xlabel("Distance")
pylab.ylabel("Freq")
pylab.show()


