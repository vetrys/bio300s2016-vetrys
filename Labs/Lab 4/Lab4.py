#*************************************
# Honor Code: This work is mine unless otherwise cited.
# Schuyler Vetry
# CMPSC 300 Spring 2016
# Lab 4
# Date: February 15, 2016
# Purpose: This program is suppose to look in depth at SREBF1 and looking at the ORFs.
# The template and basics of this program was taken from the String example in the CS crash course and the biopython program. 
# I worked on this with Leah and Julie. 
#*************************************
from Bio import SeqIO

my_file = open('SREBF1.fasta')
count = 0
name = ""

for record in SeqIO.parse(my_file,'fasta'):
	name = record.id
	seq = record.seq
	print 'Name:' , name
	print 'Size: ', len(seq)
	
my_file.close()
	

print 'First ATG is found at position: ', seq.find('ATG')
print 'Number of ATG is: ', seq.count('ATG')
