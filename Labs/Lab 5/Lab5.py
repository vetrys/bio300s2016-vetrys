#####################################################
#Schuyler Vetry
#Bio/Compsci300
#March 10 2016
#Lab 5 
#This work is mine unless otherwise cited. 
###########################################################

from string import maketrans

def main():

	file1 = open('mutation1.txt')
	file2 = open('mutation2.txt')

	file1_name = file1.readline()
	file2_name = file2.readline()
	file1 = ''.join(file1)
	file2 = ''.join(file2)
	file1 = file1.replace('\n','').upper()
	file2 = file2.replace('\n','').upper()

	file1_transcribed = file1.translate(maketrans('ACTG', 'UGAC'))
	file2_transcribed = file2.translate(maketrans('ACTG', 'UGAC'))

	print file1_transcribed
	print file2_transcribed

if __name__ == '__main__':main()

