from Bio.Align.Applications import MuscleCommandline

# by default MUSCLE will write the alignment to standard output (stdout)
# this gives a string as the output
muscle_cline = MuscleCommandline(input="ermBsequences.txt") #or output to the file by: out="new_ermb.fasta")
stdout, stderr = muscle_cline()

# use StringIO to turn the string into a handle
from StringIO import StringIO
from Bio import AlignIO
align = AlignIO.read(StringIO(stdout), "fasta")
print align
